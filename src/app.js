const express = require('express');
const http = require('http');

const configJson = require('./config.json');
const zoomRoute = require('./routes/zoomRoutes')
const bodyParser= require('body-parser')

const app = express()

const port = process.env.PORT || 8055
app.use(bodyParser.json({ limit: '5mb' }))
app.use(bodyParser.urlencoded({ limit: '5mb', extended: false }))

app.use('/', zoomRoute)
app.set('port', port)
let  server = http.createServer(app)

server.listen(port, function() {
			console.log(`Web Server started on port ${port}`)
			server.emit('appStarted')
})

module.exports = server
