const express = require('express');

var zoomRouter = express.Router();

zoomRouter.get('/',  function(req, res) {

    res.send('Hello word ' + req.query);

})
zoomRouter.get('/healthcheck', async function(req,res) {
        return res.status(200).send({});
})

zoomRouter.post('/inputs/zoom/event',  function(req, res) {

    console.log("Input query: "+ JSON.stringify(req.query))
    console.log("Input param: "+ JSON.stringify(req.param))
    console.log("Input body: "+ JSON.stringify(req.body))
    res.send('post API');

})

module.exports = zoomRouter
